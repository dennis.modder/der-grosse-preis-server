
const path = require('path');
const express = require('express');
const app = express();

const p = path.join(__dirname, './app');
const port = 10001;

app.use('/', express.static(p));

app.listen(port, () => {
    console.log('http://localhost:' + port);
});
